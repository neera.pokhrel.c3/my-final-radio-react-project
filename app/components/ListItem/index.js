import React from 'react';
import PropTypes from 'prop-types';
import Item from './Item';
import Wrapper from './Wrapper';
import ListItemInner from "../ListItemInner/index";

function ListItem(props) {
  var divStyle ={
    padding: '1000px',
  }
  debugger;
  return (
    <Wrapper>
      <Item>
        {props.item}
        <ListItemInner data={props.item.props.data} onClick={props.item.props.onClick}/>
      </Item>
    </Wrapper>
  );
}

ListItem.propTypes = {
  item: PropTypes.any,
};

export default ListItem;

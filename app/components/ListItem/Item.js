import styled from 'styled-components';

const Item = styled.div`
  display: flex;
  background: white;
  justify-content: space-between;
  padding: 2em;
  width: 100%;
  height: 100%;
  align-items: center;
  font
`;

export default Item;

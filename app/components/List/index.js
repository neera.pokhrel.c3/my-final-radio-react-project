import React from 'react';
import PropTypes from 'prop-types';
import { ListGroup, Panel, ListGroupItem, Table, Button, ButtonToolbar } from 'react-bootstrap';

import Wrapper from './Wrapper';

function List(props) {
  const ComponentToRender = props.component;
  let content = (<div></div>);
  // If we have items, render them
  if (props.items) {
    content = props.items.map((item) => (
      <ComponentToRender key={`item-${item.key}`} item={item} />
    ));
    debugger;
  } else {
    // Otherwise render a single component
    content = (<ComponentToRender />);
  }

  return (
    <Panel bsStyle="primary">
      <Panel.Heading>
        <Panel.Title componentClass="h3">List of stations</Panel.Title>
      </Panel.Heading>
      <Panel.Body>
        <Table responsive>
          <thead>
            <tr>
            </tr>
          </thead>
          <tbody>
            <tr style={{ width: '100%' }}>
              {content}
            </tr>
          </tbody>
        </Table>
      </Panel.Body>

    </Panel>
  );
}

List.propTypes = {
  component: PropTypes.func.isRequired,
  itemType: PropTypes.string,
  items: PropTypes.array,
};

export default List;

/**
*
* ListItemInner
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Button, ButtonToolbar} from 'react-bootstrap'


function ListItemInner(props) {
  return (
    <div>
      <ButtonToolbar>
        <Button bsStyle="primary" id={props.data} onClick={props.onClick}>Edit</Button>
      </ButtonToolbar>
    </div>
  );
}

ListItemInner.propTypes = {
  data: PropTypes.any,
  onClick:PropTypes.func,

};

export default ListItemInner;

import React from 'react';
import PropTypes from 'prop-types';

import List from 'components/List';
import ListItem from 'components/ListItem';
import LoadingIndicator from 'components/LoadingIndicator';
// import RepoListItem from 'containers/RepoListItem';

function ReposList({ loading, error, success }) {
  if (loading) {
    return <List component={LoadingIndicator} />;
  }

  if (error !== false) {
    const ErrorComponent = () => (
      <ListItem item={'Something went wrong, please try again!'} />
    );
    return <List component={ErrorComponent} />;
  }

  if ( success !==false) {
    const SuccessComponent = () => (
      <ListItem item={'Your login succeed'} />
    );
    return <List component={SuccessComponent} />;
  }

  // if (repos !== false) {
  //   return <List items={repos} component={RepoListItem} />;
  // }

  return null;
}

ReposList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  success: PropTypes.any,
};

export default ReposList;

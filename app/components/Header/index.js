import React from 'react';
import {Nav, NavDropdown,NavItem, MenuItem, Navbar} from 'react-bootstrap'
import { FormattedMessage } from 'react-intl';

// import A from './A';
// import Img from './Img';
// import NavBar from './NavBar';
// import HeaderLink from './HeaderLink';
// import Banner from './banner.jpg';
// import messages from './messages';
import PropTypes from 'prop-types';
import {compose} from 'redux';
import withRouter from "react-router-dom/es/withRouter";
// import withRouter from "react-router-dom/es/withRouter";
class Header extends React.Component { // eslint-disable-line react/prefer-stateless-funct

  constructor(props) {
    super(props);
    this.addStation= this.addStation
      .bind(this);
  }


  logout(){
   debugger;
   sessionStorage.setItem("loginStatus", false);
   this.props.history.push('/')

}
  addStation(){
    debugger;
    this.props.history.push('/addStations/')
  }
  render() {
    return (
      <div>
        {/*<A href="https://twitter.com/mxstbr">*/}
          {/*<Img src={Banner} alt="react-boilerplate - Logo" />*/}
        {/*</A>*/}
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#">Radio Checker System</a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem eventKey={1} href="/index">Home</NavItem>
              <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
                <MenuItem eventKey={3.1} onClick={this.addStation} href="#">Add Stations</MenuItem>
                <MenuItem eventKey={3.2}>Another action</MenuItem>
                <MenuItem eventKey={3.3}>Something else here</MenuItem>
                <MenuItem divider />
                <MenuItem eventKey={3.3}>Separated link</MenuItem>
              </NavDropdown>
              <NavItem eventKey={2} onClick={this.logout}>Logout</NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>

    );
  }
}


export default compose(
  withRouter,
)(Header);

//export default Header;


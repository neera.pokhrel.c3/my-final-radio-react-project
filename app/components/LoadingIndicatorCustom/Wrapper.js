import styled from 'styled-components';

const Wrapper = styled.div`
  margin-top: 5%;
  margin-left: 22%;
  width: 40px;
  height: 40px;
  position: absolute;
  z-index: 10000
`;

export default Wrapper;

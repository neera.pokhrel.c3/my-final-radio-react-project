/* eslint-disable */
/**
 * Created by suman on 9/1/16.
 */
const Rebase = require('re-base');

const firebase = require('firebase');
//const database = require('firebase/database');
const app = firebase.initializeApp({
  apiKey: 'AIzaSyBNNp9pCeftrASs5QRCxzC82xz7NjKI1uQ',
  authDomain: 'react-radio.firebaseapp.com',
  databaseURL: 'https://react-radio.firebaseio.com',
  projectId: 'react-radio',
  storageBucket: 'react-radio.appspot.com',
  messagingSenderId: '869819680074',
});
// const db = firebase.database(app);
// const base = Rebase.createClass(db);
const base = Rebase.createClass(app.database());

// let storage = base.storage;

const model = {
  getApp() {
    return app;
  },
  getBase() {
    return base;
  },
  getLimitedStations(cb, id) {
    base.fetch('/ARkai Radios', {
      context: this,
      then: cb,
    });
  },
  getAllRadios(cb) {
    base.fetch('/', {
      context: this,
      asArray: true,
      then: cb,
    });
  },
  addStation(cb, id, station, stationId) {
    base.post(`ARkai Radios/working/${stationId + 1}`, {
      context: this,
      data: station,
      asArray: true,
      then: cb })
      .catch((err) => {
        // handle error
      });
  },

  editStation(cb, station, stationId) {
    base.post(`ARkai Radios/working/${stationId}`, {
      context: this,
      data: station,
      asArray: true,
      then: cb })
      .catch((err) => {
        // handle error
      });
  },
  // var immediatelyAvailableReference = base.push('ARkai Radios/working',{
  //   context: this,
  //   data: station,
  //   asArray: true,
  //   then:cb})
  //   .catch(err => {
  //     //handle error
  //   });
  // available immediately, you don't have to wait for the Promise to resolve

  // },

  unAuth() {
    base.unauth((data) => {
      console.log('un-auth user', data);
    });
  },
  checkUser(callback, username, password) {
    return app.auth().signInWithEmailAndPassword(username, password).then(callback).catch(callback);
  },

  // checkSessionUser(){
  //
  // },
  checkUser2(callback, username, password) {

    // debugger;
  },
  getAllUsers(callback) {
    base.fetch('user/', {
      context: this,
      asArray: true,
      then: callback,
    });
  },
  removeImaegeByName(fileName) {
    const storageRef = base.storage().ref();
    const imageRef = storageRef.child(fileName);
// Delete the file
    imageRef.delete().then(() => {
      // console.log('successfully deleted ', fileName);
    }).catch((error) => {
      console.log('error while deleting file deleted ', fileName, error);
    });
  },
  removeStationByKey(key, callback) {
    const databaseRef = base.database().ref();
    const postRef = databaseRef.child(`stations/albanian_radio/, ${key}`);
    postRef.remove(callback);
  },
  getStationById(callback,stationId) {
    base.fetch(`ARkai Radios/working/${stationId}`, {
      context: this,
      then: callback,
    });
  },

};

module.exports = model;

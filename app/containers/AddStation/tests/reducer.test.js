
import { fromJS } from 'immutable';
import addStationReducer from '../reducer';

describe('addStationReducer', () => {
  it('returns the initial state', () => {
    expect(addStationReducer(undefined, {})).toEqual(fromJS({}));
  });
});

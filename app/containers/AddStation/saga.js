/* eslint-disable */
// import { take, call, put, select } from 'redux-saga/effects';

// Individual exports for testing


import { ADD_RADIO_STATION } from './constants';
import { put, select, takeLatest, take, call } from 'redux-saga/effects';
import { makeSelectAddRadioId } from '../AddStation/selectors';
import { makeSelectAddImage, makeSelectAddStationName, makeSelectAddStationSize, makeSelectAddUrl } from './selectors';
import * as models from '../../utils/model';
import { addRadioSize, addSuccessStatus } from './actions';


export function* addRadioStationData() {
  const radioSize = sessionStorage.getItem('radioSize');
  const station = {
    radio_title: yield select(makeSelectAddStationName()),
    key: radioSize,
    stream_url: yield select(makeSelectAddUrl()),
    image_url: yield select(makeSelectAddImage()),

  };
  debugger;
  const radioId = sessionStorage.getItem('radioId');

  try {
    const addStatus = yield call(() => new Promise((resolve, reject) => {
      const handelCallback = function (addStatus) {
        resolve(addStatus);
      };
      models.addStation(handelCallback, radioId, station, radioSize);
    }));
    // yield put(addRadioSize(toString(radioSize+1)));
    // yield put (addRadioSize(radioSize+1));
    yield put(addSuccessStatus('Station added successfully.'));
    yield put(addSuccessStatus('Station added successfully!'));
    // debugger;
  } catch (err) {
    console.log('error error');
  }
}

export default function* defaultSaga() {
  yield takeLatest(ADD_RADIO_STATION, addRadioStationData);
}

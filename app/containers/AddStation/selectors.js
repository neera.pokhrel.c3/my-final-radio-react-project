import { createSelector } from 'reselect';

/**
 * Direct selector to the addStation state domain
 */
const selectAddStationDomain = (state) => state.get('addStation');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AddStation
 */

// const makeSelectAddStation = () => createSelector(
//   selectAddStationDomain,
//   (substate) => substate.toJS()
// );

const makeSelectAddStationName = ()=> createSelector(
  selectAddStationDomain,
  (substate) => substate.get('stationName')
);

const makeSelectAddUrl = ()=> createSelector(
  selectAddStationDomain,
  (substate) => substate.get('stationUrl')
);

const makeSelectAddImage = ()=> createSelector(
  selectAddStationDomain,
  (substate) => substate.get('stationImage')
);
const makeSelectSuccessStatus= ()=> createSelector(
  selectAddStationDomain,
  (substate)=> substate.get('successStatus')
)

export {
  // makeSelectAddStation,
  selectAddStationDomain,
  makeSelectAddImage,
  makeSelectAddUrl,
  makeSelectAddStationName,
  makeSelectSuccessStatus,
};

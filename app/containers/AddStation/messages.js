/*
 * AddStation Messages
 *
 * This contains all the text for the AddStation component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AddStation.header',
    defaultMessage: 'This is AddStation container !',
  },
});

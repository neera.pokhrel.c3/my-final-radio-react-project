/*
 *
 * AddStation constants
 *
 */

export const DEFAULT_ACTION = 'app/AddStation/DEFAULT_ACTION';
export const ADD_STATION_NAME = 'app/AddStation/ADD_STATION_NAME';
export const ADD_STATION_URL= 'app/AddStation/ADD_STATION_URL';
export const ADD_STATION_IMAGE= 'app/AddStation/ADD_STATION_IMAGE';
export const ADD_RADIO_STATION ='app/AddStation/ADD_RADIO_STATION';
export const GET_RADIO_STATUS = 'app/AddStation/GET_RADIO_STATUS';

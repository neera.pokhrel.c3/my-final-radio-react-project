/* eslint-disable */
/**
 *
 * AddStation
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
// import {Button} from 'react-bootstrap';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import {
  makeSelectAddStation, makeSelectAddStationName, makeSelectAddUrl, makeSelectAddImage,
  makeSelectAddRadioId, makeSelectAddStationSize, makeSelectSuccessStatus,
} from './selectors';

// React router version v4
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom';

import reducer from './reducer';
import saga from './saga';
// import messages from './messages';
import Header from '../../components/Header/index';
import {
  addRadioId, addRadioSize, addRadioStation, changeStationImage, changeStationName,
  changeStationUrl, clearAllStates,
} from './actions';
import withRouter from 'react-router-dom/es/withRouter';

export class AddStation extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentWillReceiveProps(newProps) {
    // if station save success
    console.log('test');
    console.log(newProps);

    if (newProps.successStatus == 'Station added successfully.') {
      // newProps.successStatus ="asdf";
      console.log('saved success');
      sessionStorage.setItem('radioSize',parseInt(sessionStorage.getItem('radioSize'))+ 1);
    }
    // console.log(newProps.stationSize);

    // this.props.history.goBack();
    //
    //
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>AddStation</title>
          <meta name="description" content="Description of AddStation" />
        </Helmet>
        <Header />
        <form type="action" onSubmit={this.props.onSubmitForm}>
          <div className="form-group">
            <label htmlFor="name">Station Name</label>
            <input
              type="text" className="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Station "
              onChange={this.props.onChangeStationName} required
            />
          </div>
          <div className="form-group">
            <label htmlFor="url">Station Url</label>
            <input
              type="text" className="form-control" id="url" aria-describedby="emailHelp" placeholder="Enter Url "
              onChange={this.props.onChangeStationUrl} required
            />
          </div>
          <div className="form-group">
            <label htmlFor="image">Station Image</label>
            <input
              type="text" id="image" className="form-control" aria-describedby="emailHelp" placeholder="Enter Image Url "
              onChange={this.props.onChangeStationImage} required
            />
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
          <p>{this.props.successStatus }</p>
        </form>
      </div>
    );
  }
}

AddStation.propTypes = {
  dispatch: PropTypes.func,
  onSubmitForm: PropTypes.func,
  onChangeStationName: PropTypes.func,
  onChangeStationUrl: PropTypes.func,
  onChangeStationImage: PropTypes.func,
  stationName: PropTypes.string,
  url: PropTypes.string,
  successStatus: PropTypes.string,
  image: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  stationName: makeSelectAddStationName(),
  url: makeSelectAddUrl(),
  image: makeSelectAddImage(),
  successStatus: makeSelectSuccessStatus(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeStationName: (evt) => dispatch(changeStationName(evt.target.value)),
    onChangeStationUrl: (evt) => dispatch(changeStationUrl(evt.target.value)),
    onChangeStationImage: (evt) => dispatch(changeStationImage(evt.target.value)),
    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) {
        evt.target.reset();
        evt.preventDefault();
      }
      dispatch(addRadioStation());
    },

  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'addStation', reducer });
const withSaga = injectSaga({ key: 'addStation', saga });

export default compose(
  withReducer,
  withSaga,
  withRouter,
  withConnect,
)(AddStation);

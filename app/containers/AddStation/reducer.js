/*
 *
 * AddStation reducer
 *
 */

import { fromJS } from 'immutable';
import {
  ADD_RADIO_ID, ADD_RADIO_SIZE,
  ADD_RADIO_STATION,
  ADD_STATION_IMAGE,
  ADD_STATION_NAME, ADD_STATION_URL, CLEAR_ADD_STATION_STATE,
  DEFAULT_ACTION, GET_RADIO_STATUS,
} from './constants';

const initialState = fromJS({});

function addStationReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_RADIO_STATION:
      return state;
    case ADD_STATION_NAME:
      return state
        .set('stationName', action.stationName);
    case ADD_STATION_URL:
      return state
        .set('stationUrl', action.stationUrl);
    case ADD_STATION_IMAGE:
      return state
        .set('stationImage', action.stationImage);
    case GET_RADIO_STATUS:
      return state
        .set('successStatus', action.successStatus);
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default addStationReducer;

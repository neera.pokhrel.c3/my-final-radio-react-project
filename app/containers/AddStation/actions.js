/*
 *
 * AddStation actions
 *
 */

import {
  ADD_RADIO_ID, ADD_RADIO_SIZE,
  ADD_RADIO_STATION,
  ADD_STATION_IMAGE,
  ADD_STATION_NAME, ADD_STATION_URL,
  DEFAULT_ACTION, GET_RADIO_STATUS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function changeStationName(stationName) {
  return {
    type: ADD_STATION_NAME,
    stationName,
  };
}

export function changeStationUrl(stationUrl) {
  return {
    type: ADD_STATION_URL,
    stationUrl,
  };
}

export function changeStationImage(stationImage) {
  return {
    type: ADD_STATION_IMAGE,
    stationImage,
  };
}

export function addRadioStation() {

  return {
    type: ADD_RADIO_STATION,
  };
}


export function addSuccessStatus(successStatus) {
  return{
    type: GET_RADIO_STATUS,
    successStatus
  };

}

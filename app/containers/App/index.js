/* eslint-disable */
/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';


import NotFoundPage from 'containers/NotFoundPage/Loadable';

import Login from 'containers/Login/Loadable';
import MainPage from 'containers/MainPage/Loadable';
import StationList from 'containers/StationList/Loadable';
import AddStation from 'containers/AddStation/Loadable';
import EditStation from 'containers/EditStation/Loadable';


const AppWrapper = styled.div`
  max-width: calc(768px + 16px * 2);
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  padding: 0 16px;
  flex-direction: column;
`;

export default function App() {
  function isLoggedIn() {
    if (sessionStorage.getItem('loginStatus')) {
      return true;
    }
  }

  return (
    <AppWrapper>
      <Helmet
        titleTemplate="%s - React.js Boilerplate"
        defaultTitle="React.js Boilerplate"
      >
        <meta name="description" content="A React.js Boilerplate application" />
      </Helmet>
      {/* <Header />*/}
      <Switch>
        <Route exact path="/" component={Login} />
        <Route
          exact path="/index" render={() => (
          isLoggedIn() ? (
            <MainPage />
          ) : (
            <Redirect to="/" />
          )
        )}
        />
        <Route
          exact path="/stationList/" render={() => (
          isLoggedIn() ? (
            <StationList />
          ) : (
            <Redirect to="/" />
          )
        )}
        />
        <Route
          exact path="/addStations/" render={() => (
          isLoggedIn() ? (
            <AddStation />
          ) : (
            <Redirect to="/" />
          )
        )}
        />
        <Route
          exact path="/stationList/edit" render={() => (
          isLoggedIn() ? (
            <EditStation />
          ) : (
            <Redirect to="/" />
          )
        )}
        />
        <Route path="" component={NotFoundPage} />
      </Switch>
      {/* // <Footer />*/}
    </AppWrapper>
  );
}


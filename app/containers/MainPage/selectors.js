import { createSelector } from 'reselect';

/**
 * Direct selector to the mainPage state domain
 */
const selectMainPageDomain = (state) => state.get('mainPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by MainPage
 */

const makeSelectStations = () => createSelector(
  selectMainPageDomain,
  ( substate ) => substate.get('stations')

);

const makeSelectRadioId= () => createSelector(
  selectMainPageDomain,
  ( substate ) => substate.get('radioId')

);
//
// const makeSelectMainPage = () => createSelector(
//   selectMainPageDomain,
//   (substate) => substate.toJS()
// );

// export default makeSelectMainPage;
export {
  selectMainPageDomain,
  makeSelectStations,
  makeSelectRadioId
  // makeSelectMainPage
};

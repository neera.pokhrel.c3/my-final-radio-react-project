import {GET_RADIOS_ACTION, LOADED_RADIOS_ACTION} from "./constants";
import {put, select, takeLatest,take, call } from 'redux-saga/effects';
import {getRadios, loadedRadios} from "./actions";

let models = require('../../utils/model');

// Individual exports for testing
export function* getRadioData() {

  var stations = yield call(function () {
    return new Promise(function (resolve, reject) {
      let handelCallback = function (stations) {
        resolve(stations)
      };
      models.getAllRadios(handelCallback)
    })
  });

// yield put({type: LOADED_RADIOS_ACTION, stations});
  yield put(loadedRadios(stations));
}
export default function* radioData() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_RADIOS_ACTION, getRadioData);

}



/*
 *
 * MainPage actions
 *
 */

import {
  DEFAULT_ACTION, SET_RADIO_ID, GET_RADIOS_ACTION, GET_USER_ID, LOADED_RADIOS_ACTION
} from './constants';


export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getRadios() {
  return{
    type: GET_RADIOS_ACTION,
  }
}

export function loadedRadios(radios) {
  return{
    type: LOADED_RADIOS_ACTION,
    radios
  }
}

export function setRadioId(radioId) {
  return{
    type: SET_RADIO_ID,
    radioId
  }

}

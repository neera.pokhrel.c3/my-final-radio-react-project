/*
 *
 * MainPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_RADIOS_ACTION, LOADED_RADIOS_ACTION, SET_RADIO_ID
} from './constants';

const initialState = fromJS({
  stations: null,
  loading: true,
  error: false,
});

function mainPageReducer(state = initialState, action) {
  switch (action.type) {
    case GET_RADIOS_ACTION:
      return state
        .set('loading', true)
        .set('error', false)
    case LOADED_RADIOS_ACTION:
      return state
        .set('loading', false)
        .set('error', false)
        .set('stations',action.radios)
    case SET_RADIO_ID:
      return state
        .set('radioId',action.radioId)

    default:
      return state;
  }
}

export default mainPageReducer;

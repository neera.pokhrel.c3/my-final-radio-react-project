/* eslint-disable */
/**
 *
 * MainPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectStations, makeSelectRadioId } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import List from '../../components/List/index';
import ListItem from '../../components/ListItem/index';
import Header from '../../components/Header';
// import {selectStations} from "./selectors";
import { setRadioId, getRadios } from './actions';
import withRouter from 'react-router-dom/es/withRouter';

const models = require('../../utils/model');

export class MainPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    // debugger;
    this.props.onLoadStations();

  // var handelCallback = function (stations) {
  //   // debugger;
  // }
  //
  //  models.getAllStation(handelCallback);
  }


  componentWillReceiveProps(newProps) {
    // debugger;
  }

  onClickEdit(evt) {
   // debugger;
    sessionStorage.setItem('radioId', evt.target.id);
    this.props.history.push('/stationList/');
  }

  render() {
    let items = [];
    let component = function () {
    };
    if (this.props.stations) {
      items = this.props.stations;
      // debugger;
      component = ({ item }) => <ListItem item={<div onClick={this.onClickEdit.bind(this)} data={item.key} >{item.key} </div>}></ListItem>;
    }
    return (
      <div>
        <Helmet>
          <title>MainPage</title>
          <meta name="description" content="Description of MainPage" />
        </Helmet>
        <Header />
        <List items={items} component={component} content={component}>
          {this.props.content}
        </List>
        <FormattedMessage {...messages.header} />
      </div>
    );
  }
}

MainPage.propTypes = {
  dispatch: PropTypes.func,
  radioId: PropTypes.string,
  stations: PropTypes.array,
  onEditRadio: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  stations: makeSelectStations(),
  radioId: makeSelectRadioId(),


});

function mapDispatchToProps(dispatch) {
  return {
    onLoadStations: () => dispatch(getRadios()),
    onEditRadio: (id) => dispatch(setRadioId(id)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'mainPage', reducer });
const withSaga = injectSaga({ key: 'mainPage', saga });

export default compose(
  withReducer,
  withRouter,
  withSaga,
  withConnect,
)(MainPage);

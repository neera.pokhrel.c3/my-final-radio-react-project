/*
 *
 * MainPage constants
 *
 */

export const DEFAULT_ACTION = 'app/MainPage/DEFAULT_ACTION';
export const GET_RADIOS_ACTION = 'app/MainPage/GET_RADIOS_ACTION'
export const LOADED_RADIOS_ACTION = 'app/MainPage/LOADED_RADIOS_ACTION'
export const SET_RADIO_ID  = 'app/MainPage/SET_RADIO_ID'



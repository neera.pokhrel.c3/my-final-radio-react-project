/*
 * EditStation Messages
 *
 * This contains all the text for the EditStation component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.EditStation.header',
    defaultMessage: 'This is EditStation container !',
  },
});

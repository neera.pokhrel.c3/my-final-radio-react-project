/* eslint-disable */
/*
 *
 * EditStation reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION, GET_STATION_DATA, LOAD_STATION_DATA,
  EDIT_RADIO_STATION,
  ADD_STATION_IMAGE,
  ADD_STATION_NAME, ADD_STATION_URL, GET_RADIO_STATUS,
} from './constants';

const initialState = fromJS({});

function editStationReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case EDIT_RADIO_STATION:
      return state;
    case ADD_STATION_NAME:
      return state
        .set('stationName', action.stationName);
    case ADD_STATION_URL:
      return state
        .set('stationUrl', action.stationUrl);
    case ADD_STATION_IMAGE:
      return state
        .set('stationImage', action.stationImage);
    case GET_RADIO_STATUS:
      return state
        .set('successStatus', action.successStatus);
    case GET_STATION_DATA:
      return state;
    case LOAD_STATION_DATA:
      return state
        .set('station', action.stationData);
    default:
      return state;
  }
}

export default editStationReducer;

/*
 *
 * EditStation constants
 *
 */

export const DEFAULT_ACTION = 'app/EditStation/DEFAULT_ACTION';
export const GET_STATION_DATA = 'app/EditStation/GET_STATION_DATA';
export const LOAD_STATION_DATA = 'app/EditStation/LOAD_STATION_DATA';
export const ADD_STATION_NAME = 'app/AddStation/ADD_STATION_NAME';
export const ADD_STATION_URL= 'app/AddStation/ADD_STATION_URL';
export const ADD_STATION_IMAGE= 'app/AddStation/ADD_STATION_IMAGE';
export const EDIT_RADIO_STATION ='app/AddStation/EDIT_RADIO_STATION';
export const GET_RADIO_STATUS = 'app/AddStation/GET_RADIO_STATUS';


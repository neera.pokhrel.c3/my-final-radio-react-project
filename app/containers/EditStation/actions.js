/* eslint-disable */
/*
 *
 * EditStation actions
 *
 */

import {
  DEFAULT_ACTION, GET_STATION_DATA, LOAD_STATION_DATA,
  EDIT_RADIO_STATION,
  ADD_STATION_IMAGE,
  ADD_STATION_NAME, ADD_STATION_URL, GET_RADIO_STATUS,
} from './constants';
import { LOAD_STATIONS_ACTION } from '../StationList/constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getStationData() {
  return {
    type: GET_STATION_DATA,
  };
}

export function loadStationData(stationData) {
  return {
    type: LOAD_STATION_DATA,
    stationData,
  };
}

export function changeStationName(stationName) {
  return {
    type: ADD_STATION_NAME,
    stationName,
  };
}

export function changeStationUrl(stationUrl) {
  return {
    type: ADD_STATION_URL,
    stationUrl,
  };
}

export function changeStationImage(stationImage) {
  return {
    type: ADD_STATION_IMAGE,
    stationImage,
  };
}

export function editRadioStation() {
  return {
    type: EDIT_RADIO_STATION,
  };
}

export function addSuccessStatus(successStatus) {
  return {
    type: GET_RADIO_STATUS,
    successStatus
  };
}


import { createSelector } from 'reselect';

/**
 * Direct selector to the editStation state domain
 */
const selectEditStationDomain = (state) => state.get('editStation');

/**
 * Other specific selectors
 */


/**
 * Default selector used by EditStation
 */

const makeSelectEditStation = () => createSelector(
  selectEditStationDomain,
  (substate) => substate.toJS()
);

const makeSelectAddStationName = ()=> createSelector(
  selectEditStationDomain,
  (substate) => substate.get('stationName')
);

const makeSelectAddUrl = ()=> createSelector(
  selectEditStationDomain,
  (substate) => substate.get('stationUrl')
);

const makeSelectAddImage = ()=> createSelector(
  selectEditStationDomain,
  (substate) => substate.get('stationImage')
);

const makeSelectStationData = () => createSelector(
  selectEditStationDomain,
  (substate) => substate.get('station')
);

const makeSelectSuccessStatus= ()=> createSelector(
  selectEditStationDomain,
  (substate)=> substate.get('successStatus')
);

export default makeSelectEditStation;
export {
  selectEditStationDomain,
  makeSelectStationData,
  makeSelectAddStationName,
  makeSelectAddUrl,
  makeSelectAddImage,
  makeSelectSuccessStatus,
};

/* eslint-disable */
// import { take, call, put, select } from 'redux-saga/effects';

// Individual exports for testing
import {EDIT_RADIO_STATION, GET_STATION_DATA} from './constants';
import { makeSelectAddImage, makeSelectAddStationName, makeSelectAddUrl } from './selectors';
import { put, select, takeLatest, take, call } from 'redux-saga/effects';
import * as models from '../../utils/model';
import {addSuccessStatus, loadStationData} from "./actions";


export function* editStationData() {
  const stationId = sessionStorage.getItem('stationId');
    debugger;
  try {
    const stationData = yield call(() => new Promise((resolve, reject) => {
      const handelCallback = function (stationData) {
        resolve(stationData);
      };
      models.getStationById(handelCallback, stationId);
    }));
    yield put(loadStationData(stationData));
  } catch (err) {
    console.log('error error');
  }
}

export function* addRadioStationData() {
  const stationId = parseInt(sessionStorage.getItem('stationId'));
  debugger;
  const station = {
    radio_title: yield select(makeSelectAddStationName()),
    key: stationId ,
    stream_url: yield select(makeSelectAddUrl()),
    image_url: yield select(makeSelectAddImage()),

  };
  try {
    const addStatus = yield call(() => new Promise((resolve, reject) => {
      const handelCallback = function (addStatus) {
        resolve(addStatus);
      };
      models.editStation(handelCallback,station, stationId);
    }));
    // yield put(addRadioSize(toString(radioSize+1)));
    // yield put (addRadioSize(radioSize+1));
    yield put(addSuccessStatus('Station edited successfully.'));

    // debugger;
  } catch (err) {
    console.log('error error');
  }

}

export default function* defaultSaga() {
  yield takeLatest(GET_STATION_DATA, editStationData);
  yield takeLatest(EDIT_RADIO_STATION, addRadioStationData);
  // See example in containers/HomePage/saga.js
}

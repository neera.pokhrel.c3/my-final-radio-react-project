/* eslint-disable */
/**
 *
 * EditStation
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import  {Component} from 'react';
import ReactPlayer from 'react-player';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectEditStation, {
  makeSelectAddImage, makeSelectAddUrl, makeSelectStationData, makeSelectAddStationName,
  makeSelectSuccessStatus,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import Header from '../../components/Header/index';
import {changeStationImage, changeStationName, changeStationUrl, editRadioStation, getStationData} from "./actions";
export class EditStation extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    // debugger;
    this.props.onLoadStation();
  }

  componentWillReceiveProps(newProps){
  }

  render() {

    if (this.props.stationData) {
      debugger;
      var name = this.props.stationData.radio_title;
      var url = this.props.stationData.stream_url;
      var image = this.props.stationData.image_url;



    }

    return (
      <div>
        <Helmet>
          <title> Edit Station</title>
          <meta name="description" content="Description of AddStation" />
        </Helmet>
        <Header />
        <form type="action" onSubmit={this.props.onSubmitForm}>
          <div className="form-group">
            <label htmlFor="name">Station Name</label>
            <input
              type="text" className="form-control" id="name" aria-describedby="emailHelp"  placeholder = {name}
              onChange={this.props.onChangeStationName}
            />
          </div>
          <div className="form-group">
            <label htmlFor="url">Station Url</label>
            <input
              type="text" className="form-control" id="url" aria-describedby="emailHelp" placeholder={url}
              onChange={this.props.onChangeStationUrl}
            />
          </div>
          <div className="form-group">
            <label htmlFor="image">Station Image</label>
            <input
              type="text" id="image" className="form-control" aria-describedby="emailHelp" placeholder={image}
              onChange={this.props.onChangeStationImage}
            />
          </div>
          <button type="submit" className="btn btn-primary">Edit</button>
          <p>{this.props.successStatus}</p>
        </form>

        </div>
    );
  }
}

EditStation.propTypes = {
  dispatch: PropTypes.func,
  onLoadStation: PropTypes.func,
  stationData: PropTypes.any,
  onSubmitForm: PropTypes.func,
  onChangeStationName: PropTypes.func,
  onChangeStationUrl: PropTypes.func,
  onChangeStationImage: PropTypes.func,
  successStatus: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  editstation: makeSelectEditStation(),
  stationData: makeSelectStationData(),
  stationName: makeSelectAddStationName(),
  url: makeSelectAddUrl(),
  image: makeSelectAddImage(),
  successStatus: makeSelectSuccessStatus(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeStationName: (evt) => dispatch(changeStationName(evt.target.value)),
    onChangeStationUrl: (evt) => dispatch(changeStationUrl(evt.target.value)),
    onChangeStationImage: (evt) => dispatch(changeStationImage(evt.target.value)),
    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) {
        evt.target.reset();
        evt.preventDefault();
      }
      dispatch(editRadioStation());
    },
    onLoadStation:() => dispatch(getStationData()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'editStation', reducer });
const withSaga = injectSaga({ key: 'editStation', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(EditStation);


import { fromJS } from 'immutable';
import editStationReducer from '../reducer';

describe('editStationReducer', () => {
  it('returns the initial state', () => {
    expect(editStationReducer(undefined, {})).toEqual(fromJS({}));
  });
});

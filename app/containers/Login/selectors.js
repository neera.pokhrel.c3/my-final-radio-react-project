import { createSelector } from 'reselect';

/**
 * Direct selector to the login state domain
 */


const selectLogin = (state) => state.get('login');

const makeSelectUsername = () => createSelector(
  selectLogin,
  (loginState) => loginState.get('username')
);

const makeSelectPassword = () => createSelector(
  selectLogin,
  (loginState) => loginState.get('password')
);

 const makeSelectLoginData = () => createSelector(
  selectLogin,
 (loginState) => loginState.get('loginData')
);
const makeSelectErrorMessage = () => createSelector(
  selectLogin,
  (loginState) => loginState.get('errorMessage')
);

const makeSelectLoading = () => createSelector(
  selectLogin,
  (loginState) => loginState.get('loading')
);




export {
  makeSelectLoading,
  selectLogin,
  makeSelectUsername,
  makeSelectPassword,
  makeSelectLoginData,
  makeSelectErrorMessage,
};

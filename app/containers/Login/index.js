/**
 *
 * Login
 *
 */

import React from 'react';
import PropTypes, { instanceOf } from 'prop-types';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {compose} from 'redux';
import {Col, Form, Glyphicon} from 'react-bootstrap';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import {login, changePassword, changeUsername,} from "./actions"

import {
  makeSelectLoading,
  makeSelectUsername,
  makeSelectPassword,
  makeSelectLoginData,
  makeSelectErrorMessage
} from './selectors';
import reducer from './reducer';
import saga from './saga';

import LoadingIndicator from '../../components/LoadingIndicatorCustom';

const divStyle = {
  padding: "20px",
  position: "absolute",
  top: "25%",
  left: "25%"

};


export class Login extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {

    // this.props.username = "suman";
    // this.props.password = "Asdfsad";
    //


    if (this.props.username && this.props.password && this.props.username.trim().length > 0 && this.props.password.trim().length > 0) {
      this.props.onSubmitForm();
    }

  }


  componentWillReceiveProps(newProps) {
    if (newProps.loginData.isLoggedIn) {
      sessionStorage.setItem('loginStatus',true)
      this.props.history.push('/index')


    }

  }


  render() {
    const {loading, errorMessage} = this.props;
    debugger;
    return (

      <div className="container" style={divStyle}>

        <LoadingIndicator loading={loading}/>
        <Col xs={6} xsOffset={6}/>
        <div className="row">
          <div className="col-md-6">
            <div className="well well-sm">
              <Form className="form-horizontal" type="action" onSubmit={this.props.onSubmitForm}>
                <fieldset>
                  <legend className="text-center header">Login</legend>
                  <div className="form-group">
                    <span className="col-md-1 col-md-offset-2 text-center"><Glyphicon glyph="user bigicon"/></span>
                    <div className="col-md-8">
                      <input
                        type="email" className="form-control" id="email" placeholder="Enter your email" required
                        value={this.props.username}
                        onChange={this.props.onChangeUsername}
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <span className="col-md-1 col-md-offset-2 text-center"> <Glyphicon glyph="pencil"/></span>
                    <div className="col-md-8">
                      <input
                        type="password" className="form-control" id="pwd" placeholder="Enter your password" required
                        value={this.props.password}
                        onChange={this.props.onChangePassword}
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="col-md-12 text-center">
                      <button type="submit" className="btn btn-primary">Submit</button>
                    </div>
                  </div>
                </fieldset>
              </Form>

              <h3> {errorMessage} </h3>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  dispatch: PropTypes.func,
  onSubmitForm: PropTypes.func,
  onChangeUsername: PropTypes.func,
  onChangePassword: PropTypes.func,
  loading: PropTypes.bool,
  username: PropTypes.string,
  password: PropTypes.string,
  loginData: PropTypes.object,
  errorMessage: PropTypes.string,
};


const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  username: makeSelectUsername(),
  password: makeSelectPassword(),
  loginData: makeSelectLoginData(),
  errorMessage: makeSelectErrorMessage(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: (evt) => dispatch(changeUsername(evt.target.value)),
    onChangePassword: (evt) => dispatch(changePassword(evt.target.value)),
    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(login());
    },
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({key: 'login', reducer});
const withSaga = injectSaga({key: 'login', saga});

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Login);

/*
 *
 * Login constants
 *
 */

export const LOGIN_ACTION = 'app/Login/LOGIN_ACTION';
export const LOGIN_AUTHENTICATE = 'app/Login/LOGIN_AUTHENTICATE'
export const LOGIN_SUCCESS ='app/Login/LOGIN_SUCCESS';
export const LOGIN_FAILED ='app/Login/LOGIN_FAILED';
export const CHANGE_USERNAME ='app/Login/CHANGE_USERNAME';
export const CHANGE_PASSWORD = 'app/Login/CHANGE_PASSWORD';


/* eslint-disable linebreak-style */
/*
 *
 * Login actions
 *
 */

import  {
  LOGIN_ACTION, LOGIN_SUCCESS, CHANGE_USERNAME, CHANGE_PASSWORD, LOGIN_FAILED, LOGIN_AUTHENTICATE
} from './constants';


export function changeUsername(name) {
  return {
    type: CHANGE_USERNAME,
    name,
  };
}


export function changePassword(password) {
  return {
    type: CHANGE_PASSWORD,
    password,
  };
}

export function login() {
  return {
    type: LOGIN_ACTION,
  };
}

export function loginAuth(username,password) {
  return{
    type: LOGIN_AUTHENTICATE,
    username,
    password,
  }

}

export function loginSuccess(loginData) {

  return{
    type: LOGIN_SUCCESS,
    loginData
  }
}
export function loginFailed(errorMessage){

  return{
    type: LOGIN_FAILED,
    errorMessage,
  }
}


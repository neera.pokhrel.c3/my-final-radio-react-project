/*
 *
 * Login reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOGIN_ACTION, LOGIN_SUCCESS, CHANGE_USERNAME, CHANGE_PASSWORD, LOGIN_FAILED, LOGIN_AUTHENTICATE,
} from './constants';

const initialState = fromJS({
  username: '',
  password: '',
  loginData: [],
  errorMessage:'',
  loading: false,
  error: false,
});

function loginReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_USERNAME:
      return state
        .set('loading', false)
        .set('username', action.name);
    case CHANGE_PASSWORD:
      return state
        .set('password', action.password);
    case LOGIN_ACTION:
      return state
        .set('loading', true)
        .set('error', false);
    case LOGIN_AUTHENTICATE:
      return state
      .set('loading', true)
      .set('username', action.username)
      .set('password', action.password)
    case LOGIN_SUCCESS:
      return state
        .set('loading', false)
        .set('loginData',{isLoggedIn: true, username: action.username})
        .set('cookies',action.username)
        .set('errorMessage','');
    case LOGIN_FAILED:
      return state
        .set('loading', false)
        .set('errorMessage', "login failed");
    default:
      return state;
  }
}

export default loginReducer;

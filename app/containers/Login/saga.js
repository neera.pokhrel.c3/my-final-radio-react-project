// import { take, call, put, select } from 'redux-saga/effects';

import { put, select, takeLatest, call } from 'redux-saga/effects';
import { LOGIN_ACTION } from './constants';
import {loginSuccess, loginFailed, loginAuth} from "./actions";
import { repoLoadingError } from "containers/App/actions";
import {makeSelectUsername, makeSelectPassword, makeSelectLoginData, makeSelectErrorMessage} from "./selectors"
let models = require('../../utils/model');


export function* getLogin() {
  const username = yield select(makeSelectUsername());
  const password = yield select(makeSelectPassword());
  const loginData = yield select(makeSelectLoginData());
  const errorMsg = yield select(makeSelectErrorMessage());


    // Call our request helper (see 'utils/request')
    var users = yield call(function () {
      return new Promise(function (resolve, reject) {
        let handelCallback = function (users) {
          resolve(users)
        };
        models.checkUser(handelCallback,username,password)
      })
    });

    if (!users.code){
          console.log('your login has succeed');
          yield put(loginSuccess(loginData));
          // yield put (loginAuth(username,password))

    }else{
          console.log('login failed');
          yield put(loginFailed("555552"));
        }


}
// Individual exports for testing
export default function* loginData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOGIN_ACTION, getLogin);
}

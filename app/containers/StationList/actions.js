/* eslint-disable */
/*
 *
 * StationList actions
 *
 */

import {
  DEFAULT_ACTION, GET_STATIONS_ACTION, LOAD_STATIONS_ACTION,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getStations() {
  return{
      type: GET_STATIONS_ACTION,
  };

}

export function loadStations(stations) {
  return{
    type : LOAD_STATIONS_ACTION,
    stations,
  };

}

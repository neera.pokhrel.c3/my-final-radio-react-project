
import { fromJS } from 'immutable';
import stationListReducer from '../reducer';

describe('stationListReducer', () => {
  it('returns the initial state', () => {
    expect(stationListReducer(undefined, {})).toEqual(fromJS({}));
  });
});

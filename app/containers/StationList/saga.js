/* eslint-disable */

import { put, select, takeLatest, take, call } from 'redux-saga/effects';
// Individual exports for testing
import { GET_STATIONS_ACTION } from './constants';
const models = require('../../utils/model');
import { loadStations } from './actions';

export function* getStationData() {
  const radioId = sessionStorage.getItem('radioId');
  try {
    let stations = yield call(function () {
      return new Promise(function (resolve, reject) {
        let handelCallback = function (stations) {
          resolve(stations)
        };
        models.getLimitedStations(handelCallback, radioId)
      })
    });
    // debugger;
    if (stations) {
      yield put(loadStations(stations));
    } else {
      console.log('There are no stations');
    }
  } catch (err) {
    console.log('error error');
  }
}

export default function* defaultSaga() {
  yield takeLatest(GET_STATIONS_ACTION, getStationData);
}

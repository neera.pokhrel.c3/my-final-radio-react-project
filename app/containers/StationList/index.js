/* eslint-disable */
/**
 *
 * StationList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { ListGroup, Image, Panel, ListGroupItem, Table, Button, ButtonToolbar } from 'react-bootstrap';
import { getStations } from './actions';
import ReactPlayer from 'react-player';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectStationList, { makeSelectRadioID } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import withRouter from 'react-router-dom/es/withRouter';
import ListItem from '../../components/ListItem/index';
import List from '../../components/List/index';
import Header from '../../components/Header/index';

export class StationList extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    this.props.onLoadStations();
  }

  onClickEdit(evt) {
    sessionStorage.setItem('stationId', evt.target.id);
    this.props.history.push('/stationList/edit');
  }

  checkURL(url){
   if(ReactPlayer.canPlay(url)){return "---"}
   else {return "***"}
  }

  render() {
    const imgStyle = {
      height: '48px',
      width: '54px',
    };


    let items = [];
    let component = function () {
    };
    if (this.props.stations.stations) {
      debugger;
      items = this.props.stations.stations.working;
      sessionStorage.setItem('radioSize', items.length)
      //
      component = ({ item }) => (<ListItem item={<div  data={item.key} onClick={this.onClickEdit.bind(this)} >{item.key} &nbsp; <img src={item.image_url} style={imgStyle} /> &nbsp;{item.radio_title} &nbsp; {item.stream_url}
        {this.checkURL(item.stream_url)}
        </div>}
      ></ListItem>);
    }
    return (
      <div>
        <Helmet>
          <title>MainPage</title>
          <meta name="description" content="Description of MainPage" />
        </Helmet>
        <Header />
        <List items={items} itemType="stations" component={component} content={component}>
          {this.props.content}
        </List>
        <FormattedMessage {...messages.header} />
      </div>
    );
  }
}

StationList.propTypes = {
  dispatch: PropTypes.func,
  stations: PropTypes.any,
  radioId: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  stations: makeSelectStationList(),
  radioId: makeSelectRadioID(),
});

function mapDispatchToProps(dispatch) {
  return {
    onLoadStations: () => dispatch(getStations()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'stationList', reducer });
const withSaga = injectSaga({ key: 'stationList', saga });

export default compose(
  withReducer,
  withSaga,
  withRouter,
  withConnect,
)(StationList);

import { createSelector } from 'reselect';

/**
 * Direct selector to the stationList state domain
 */
const selectStationListDomain = (state) => state.get('stationList');

/**
 * Other specific selectors
 */


/**
 * Default selector used by StationList
 */

const makeSelectStationList = () => createSelector(
  selectStationListDomain,
  (substate) => substate.toJS('stations')
);

const makeSelectRadioID = () => createSelector(
  selectStationListDomain,
  (substate) => substate.get('radioId')
);

export default makeSelectStationList;
export {
  selectStationListDomain,
  makeSelectRadioID,
  makeSelectStationList,
};

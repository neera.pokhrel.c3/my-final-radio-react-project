/*
 *
 * StationList reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION, GET_STATIONS_ACTION, LOAD_STATIONS_ACTION,
} from './constants';

const initialState = fromJS({
  stations: null,
  loading: true,
  error: false,
});

function stationListReducer(state = initialState, action) {

  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GET_STATIONS_ACTION:
      return state;
    case LOAD_STATIONS_ACTION:
      return state
        .set('stations',action.stations)
    default:
      return state;

  }
}

export default stationListReducer;

/*
 * StationList Messages
 *
 * This contains all the text for the StationList component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.StationList.header',
    defaultMessage: 'This is StationList container !',
  },
});
